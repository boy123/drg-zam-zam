<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Alat_tindakan extends CI_Controller
{
    var $judul_page = 'Alat Tindakan';
    function __construct()
    {
        parent::__construct();
        $this->load->model('Alat_tindakan_model');
        $this->load->library('form_validation');
    }

    public function get_alat($id_alat)
    {
        $this->db->where('id_alat', $id_alat);
        $data = $this->db->get('alat')->result_array();
        echo json_encode($data);
    }

    public function index()
    {
    	$id_ksm_user = $this->session->userdata('ksm');
    	$ksm_user = get_data('ksm','id_ksm',$id_ksm_user,'ksm');
    	$where = "";
    	if ($ksm_user != '') {
    		$where = "WHERE b.ksm='$ksm_user'";
    	}
    	$sql = "
    		SELECT a.*, b.ksm from alat_tindakan as a INNER JOIN tindakan as b ON a.kode_tindakan=b.kode_tindakan $where;
    	";
        $alat_tindakan = $this->db->query($sql);
        // $alat_tindakan = $this->Alat_tindakan_model->get_all();

        $data = array(
            'alat_tindakan_data' => $alat_tindakan,
            'judul_page' => $this->judul_page,
            'konten' => 'alat_tindakan/alat_tindakan_list',
        );
        $this->load->view('v_index', $data);
    }

    

    public function create() 
    {
        $data = array(
            'judul_page' => $this->judul_page,
            'konten' => 'alat_tindakan/alat_tindakan_form',
            'judul_form' => 'Tambah '.$this->judul_page,
            'button' => 'Simpan',
            'action' => site_url('alat_tindakan/create_action'),
	    'id_alat_tindakan' => set_value('id_alat_tindakan'),
	    'kode_tindakan' => set_value('kode_tindakan'),
	    'id_alat' => set_value('id_alat'),
	    'nama_alat' => set_value('nama_alat'),
	    'volume' => set_value('volume'),
	    'satuan' => set_value('satuan'),
	    'umur_ekonomis' => set_value('umur_ekonomis'),
	    'jumlah_pemakaian' => set_value('jumlah_pemakaian'),
	    'merk' => set_value('merk'),
	    'harga' => set_value('harga'),
	    'link_produk' => set_value('link_produk'),
	    'harga_perpemakaian' => set_value('harga_perpemakaian'),
	);
        $this->load->view('v_index', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'kode_tindakan' => $this->input->post('kode_tindakan',TRUE),
		'id_alat' => $this->input->post('id_alat',TRUE),
		'nama_alat' => $this->input->post('nama_alat',TRUE),
		'volume' => $this->input->post('volume',TRUE),
		'satuan' => $this->input->post('satuan',TRUE),
		'umur_ekonomis' => $this->input->post('umur_ekonomis',TRUE),
		'jumlah_pemakaian' => $this->input->post('jumlah_pemakaian',TRUE),
		'merk' => $this->input->post('merk',TRUE),
		'harga' => $this->input->post('harga',TRUE),
		'link_produk' => $this->input->post('link_produk',TRUE),
		'harga_perpemakaian' => $this->input->post('harga_perpemakaian',TRUE),
	    );

            $this->Alat_tindakan_model->insert($data);
            $this->session->set_flashdata('message', message('success','Data berhasil disimpan'));
            redirect(site_url('alat_tindakan'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Alat_tindakan_model->get_by_id($id);

        if ($row) {
            $data = array(
                'judul_page' => $this->judul_page,
                'konten' => 'alat_tindakan/alat_tindakan_form',
                'judul_form' => 'Ubah '.$this->judul_page,
                'button' => 'Update',
                'action' => site_url('alat_tindakan/update_action'),
		'id_alat_tindakan' => set_value('id_alat_tindakan', $row->id_alat_tindakan),
		'kode_tindakan' => set_value('kode_tindakan', $row->kode_tindakan),
		'id_alat' => set_value('id_alat', $row->id_alat),
		'nama_alat' => set_value('nama_alat', $row->nama_alat),
		'volume' => set_value('volume', $row->volume),
		'satuan' => set_value('satuan', $row->satuan),
		'umur_ekonomis' => set_value('umur_ekonomis', $row->umur_ekonomis),
		'jumlah_pemakaian' => set_value('jumlah_pemakaian', $row->jumlah_pemakaian),
		'merk' => set_value('merk', $row->merk),
		'harga' => set_value('harga', $row->harga),
		'link_produk' => set_value('link_produk', $row->link_produk),
		'harga_perpemakaian' => set_value('harga_perpemakaian', $row->harga_perpemakaian),
	    );
            $this->load->view('v_index', $data);
        } else {
            $this->session->set_flashdata('message', message('danger','Data tidak ditemukan'));
            redirect(site_url('alat_tindakan'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_alat_tindakan', TRUE));
        } else {
            $data = array(
		'kode_tindakan' => $this->input->post('kode_tindakan',TRUE),
		'id_alat' => $this->input->post('id_alat',TRUE),
		'nama_alat' => $this->input->post('nama_alat',TRUE),
		'volume' => $this->input->post('volume',TRUE),
		'satuan' => $this->input->post('satuan',TRUE),
		'umur_ekonomis' => $this->input->post('umur_ekonomis',TRUE),
		'jumlah_pemakaian' => $this->input->post('jumlah_pemakaian',TRUE),
		'merk' => $this->input->post('merk',TRUE),
		'harga' => $this->input->post('harga',TRUE),
		'link_produk' => $this->input->post('link_produk',TRUE),
		'harga_perpemakaian' => $this->input->post('harga_perpemakaian',TRUE),
	    );

            $this->Alat_tindakan_model->update($this->input->post('id_alat_tindakan', TRUE), $data);
            $this->session->set_flashdata('message', message('success','Data berhasil diupdate'));
            redirect(site_url('alat_tindakan'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Alat_tindakan_model->get_by_id($id);

        if ($row) {
            $this->Alat_tindakan_model->delete($id);
            $this->session->set_flashdata('message', message('success','Data berhasil dihapus'));
            redirect(site_url('alat_tindakan'));
        } else {
            $this->session->set_flashdata('message', message('danger','Data tidak ditemukan'));
            redirect(site_url('alat_tindakan'));
        }
    }

    public function import_excel()
    {
        // Fungsi untuk melakukan proses upload file
        $return = array();
        $this->load->library('upload'); // Load librari upload

        $config['upload_path'] = './files/excel/';
        $config['allowed_types'] = 'xlsx';
        $config['max_size'] = '2048';
        $config['overwrite'] = true;
        $config['file_name'] = 'import_alat_tindakan';

        $this->upload->initialize($config); // Load konfigurasi uploadnya
        if($this->upload->do_upload('file_excel')){ // Lakukan upload dan Cek jika proses upload berhasil
            // Jika berhasil :
            $return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
        }else{
            // Jika gagal :
            $return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());

            $this->session->set_flashdata('message',message($return['error'],'error'));
            redirect('alat_tindakan','refresh');
        }

		include APPPATH.'third_party/PHPExcel/PHPExcel.php';

		$filename = 'import_alat_tindakan.xlsx';
					
		$excelreader = new PHPExcel_Reader_Excel2007();
		$loadexcel = $excelreader->load('files/excel/'.$filename.''); // Load file yang tadi diupload ke folder excel
		$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);
		
        //skip untuk header
		unset($sheet[1]);

        $this->db->trans_begin();

		foreach ($sheet as $rw) {
			if (empty($rw['A'])) {
				// code...
			} else {
				$data = array(
		'kode_tindakan' => $rw['A'],
		'id_alat' => $rw['B'],
		'nama_alat' => $rw['C'],
		'volume' => $rw['D'],
		'satuan' => $rw['E'],
		'umur_ekonomis' => $rw['F'],
		'jumlah_pemakaian' => $rw['G'],
		'merk' => $rw['H'],
		'harga' => $rw['I'],
		'link_produk' => $rw['J'],
		'harga_perpemakaian' => $rw['K'],
	);

				$this->db->insert('alat_tindakan', $data);
			}
			
		}

		if ($this->db->trans_status() === FALSE)
		{
	        $this->db->trans_rollback();
	        $this->session->set_flashdata('message', message('gagal server,silahkan ulangi','error'));
			redirect('alat_tindakan','refresh');
		}
		else
		{
	        $this->db->trans_commit();
	        $this->session->set_flashdata('message', message('data berhasil diimport','success'));
			redirect('alat_tindakan','refresh');
		}

    }

    public function _rules() 
    {
	$this->form_validation->set_rules('kode_tindakan', 'kode tindakan', 'trim|required');
	$this->form_validation->set_rules('nama_alat', 'nama alat', 'trim|required');
	$this->form_validation->set_rules('volume', 'volume', 'trim|required');
	$this->form_validation->set_rules('satuan', 'satuan', 'trim|required');
	$this->form_validation->set_rules('umur_ekonomis', 'umur ekonomis', 'trim|required');
	$this->form_validation->set_rules('jumlah_pemakaian', 'jumlah pemakaian', 'trim|required');
	$this->form_validation->set_rules('merk', 'merk', 'trim|required');
	$this->form_validation->set_rules('harga', 'harga', 'trim|required');
	$this->form_validation->set_rules('link_produk', 'link produk', 'trim|required');
	$this->form_validation->set_rules('harga_perpemakaian', 'harga perpemakaian', 'trim|required');

	$this->form_validation->set_rules('id_alat_tindakan', 'id_alat_tindakan', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Alat_tindakan.php */
/* Location: ./application/controllers/Alat_tindakan.php */
/* Please DO NOT modify this information : */
/* Generated by Boy Kurniawan 2022-02-09 03:33:09 */
/* https://jualkoding.com */