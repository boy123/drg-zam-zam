<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Biaya_tidak_langsung extends CI_Controller
{
    var $judul_page = 'Biaya Tidak Langsung';
    function __construct()
    {
        parent::__construct();
        $this->load->model('Biaya_tidak_langsung_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $biaya_tidak_langsung = $this->Biaya_tidak_langsung_model->get_all();

        $data = array(
            'biaya_tidak_langsung_data' => $biaya_tidak_langsung,
            'judul_page' => $this->judul_page,
            'konten' => 'biaya_tidak_langsung/biaya_tidak_langsung_list',
        );
        $this->load->view('v_index', $data);
    }

    

    public function create() 
    {
        $data = array(
            'judul_page' => $this->judul_page,
            'konten' => 'biaya_tidak_langsung/biaya_tidak_langsung_form',
            'judul_form' => 'Tambah '.$this->judul_page,
            'button' => 'Simpan',
            'action' => site_url('biaya_tidak_langsung/create_action'),
	    'id_btl' => set_value('id_btl'),
	    'nama' => set_value('nama'),
	    'satuan' => set_value('satuan'),
	    'biaya' => set_value('biaya'),
	    'kebutuhan_perbulan' => set_value('kebutuhan_perbulan'),
	    'biaya_pertahun' => set_value('biaya_pertahun'),
	);
        $this->load->view('v_index', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'nama' => $this->input->post('nama',TRUE),
		'satuan' => $this->input->post('satuan',TRUE),
		'biaya' => $this->input->post('biaya',TRUE),
		'kebutuhan_perbulan' => $this->input->post('kebutuhan_perbulan',TRUE),
		'biaya_pertahun' => $this->input->post('biaya_pertahun',TRUE),
	    );

            $this->Biaya_tidak_langsung_model->insert($data);
            $this->session->set_flashdata('message', message('success','Data berhasil disimpan'));
            redirect(site_url('biaya_tidak_langsung'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Biaya_tidak_langsung_model->get_by_id($id);

        if ($row) {
            $data = array(
                'judul_page' => $this->judul_page,
                'konten' => 'biaya_tidak_langsung/biaya_tidak_langsung_form',
                'judul_form' => 'Ubah '.$this->judul_page,
                'button' => 'Update',
                'action' => site_url('biaya_tidak_langsung/update_action'),
		'id_btl' => set_value('id_btl', $row->id_btl),
		'nama' => set_value('nama', $row->nama),
		'satuan' => set_value('satuan', $row->satuan),
		'biaya' => set_value('biaya', $row->biaya),
		'kebutuhan_perbulan' => set_value('kebutuhan_perbulan', $row->kebutuhan_perbulan),
		'biaya_pertahun' => set_value('biaya_pertahun', $row->biaya_pertahun),
	    );
            $this->load->view('v_index', $data);
        } else {
            $this->session->set_flashdata('message', message('danger','Data tidak ditemukan'));
            redirect(site_url('biaya_tidak_langsung'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_btl', TRUE));
        } else {
            $data = array(
		'nama' => $this->input->post('nama',TRUE),
		'satuan' => $this->input->post('satuan',TRUE),
		'biaya' => $this->input->post('biaya',TRUE),
		'kebutuhan_perbulan' => $this->input->post('kebutuhan_perbulan',TRUE),
		'biaya_pertahun' => $this->input->post('biaya_pertahun',TRUE),
	    );

            $this->Biaya_tidak_langsung_model->update($this->input->post('id_btl', TRUE), $data);
            $this->session->set_flashdata('message', message('success','Data berhasil diupdate'));
            redirect(site_url('biaya_tidak_langsung'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Biaya_tidak_langsung_model->get_by_id($id);

        if ($row) {
            $this->Biaya_tidak_langsung_model->delete($id);
            $this->session->set_flashdata('message', message('success','Data berhasil dihapus'));
            redirect(site_url('biaya_tidak_langsung'));
        } else {
            $this->session->set_flashdata('message', message('danger','Data tidak ditemukan'));
            redirect(site_url('biaya_tidak_langsung'));
        }
    }

    public function import_excel()
    {
        // Fungsi untuk melakukan proses upload file
        $return = array();
        $this->load->library('upload'); // Load librari upload

        $config['upload_path'] = './files/excel/';
        $config['allowed_types'] = 'xlsx';
        $config['max_size'] = '2048';
        $config['overwrite'] = true;
        $config['file_name'] = 'import_biaya_tidak_langsung';

        $this->upload->initialize($config); // Load konfigurasi uploadnya
        if($this->upload->do_upload('file_excel')){ // Lakukan upload dan Cek jika proses upload berhasil
            // Jika berhasil :
            $return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
        }else{
            // Jika gagal :
            $return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());

            $this->session->set_flashdata('message',message($return['error'],'error'));
            redirect('biaya_tidak_langsung','refresh');
        }

		include APPPATH.'third_party/PHPExcel/PHPExcel.php';

		$filename = 'import_biaya_tidak_langsung.xlsx';
					
		$excelreader = new PHPExcel_Reader_Excel2007();
		$loadexcel = $excelreader->load('files/excel/'.$filename.''); // Load file yang tadi diupload ke folder excel
		$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);
		
        //skip untuk header
		unset($sheet[1]);

        $this->db->trans_begin();

		foreach ($sheet as $rw) {
			if (empty($rw['A'])) {
				// code...
			} else {
				$data = array(
		'nama' => $rw['A'],
		'satuan' => $rw['B'],
		'biaya' => $rw['C'],
		'kebutuhan_perbulan' => $rw['D'],
		'biaya_pertahun' => $rw['E'],
	);

				$this->db->insert('biaya_tidak_langsung', $data);
			}
			
		}

		if ($this->db->trans_status() === FALSE)
		{
	        $this->db->trans_rollback();
	        $this->session->set_flashdata('message', message('gagal server,silahkan ulangi','error'));
			redirect('biaya_tidak_langsung','refresh');
		}
		else
		{
	        $this->db->trans_commit();
	        $this->session->set_flashdata('message', message('data berhasil diimport','success'));
			redirect('biaya_tidak_langsung','refresh');
		}

    }

    public function _rules() 
    {
	$this->form_validation->set_rules('nama', 'nama', 'trim|required');
	$this->form_validation->set_rules('satuan', 'satuan', 'trim|required');
	$this->form_validation->set_rules('biaya', 'biaya', 'trim|required');
	$this->form_validation->set_rules('kebutuhan_perbulan', 'kebutuhan perbulan', 'trim|required');
	$this->form_validation->set_rules('biaya_pertahun', 'biaya pertahun', 'trim|required');

	$this->form_validation->set_rules('id_btl', 'id_btl', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Biaya_tidak_langsung.php */
/* Location: ./application/controllers/Biaya_tidak_langsung.php */
/* Please DO NOT modify this information : */
/* Generated by Boy Kurniawan 2022-02-08 02:18:26 */
/* https://jualkoding.com */