<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tindakan extends CI_Controller
{
    var $judul_page = 'Data Tindakan';
    function __construct()
    {
        parent::__construct();
        $this->load->model('Tindakan_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $id_ksm_user = $this->session->userdata('ksm');
        $ksm_user = get_data('ksm','id_ksm',$id_ksm_user,'ksm');
        $where = "";
        if ($ksm_user != '') {
            $where = "WHERE ksm='$ksm_user'";
        }
        $tindakan = $this->db->query("SELECT * FROM tindakan $where");

        $data = array(
            'tindakan_data' => $tindakan,
            'judul_page' => $this->judul_page,
            'konten' => 'tindakan/tindakan_list',
        );
        $this->load->view('v_index', $data);
    }

    public function hitung_tindakan()
    {
        $this->load->view('tindakan/hitung_tindakan2');
    }

    public function get_tindakan()
    {
        $data = array();
        $akum_total_tindakan = total_tindakan();
        // $this->db->limit(20);
        foreach ($this->db->get('tindakan')->result() as $rw) {
            array_push($data, $rw);
        }

        echo json_encode($data);
    }

    public function proses_hitung_tindakan($total_tindakan, $id_tindakan,$akum_total_tindakan, $total_btl)
    {
        $pembobotan_biaya_tidak_langsung = $total_tindakan / $akum_total_tindakan;
        $biaya_tidak_langsung_pertindakan = ($pembobotan_biaya_tidak_langsung * $total_btl) / $total_tindakan;
        $this->db->where('id_tindakan', $id_tindakan);
        $this->db->update('tindakan', [
            'pembobotan_biaya_tidak_langsung' => $pembobotan_biaya_tidak_langsung,
            'biaya_tidak_langsung_pertindakan' => $biaya_tidak_langsung_pertindakan
        ]);
        echo json_encode(array(
            'status' => 'sukses'
        ));
    }

    public function create() 
    {
        $data = array(
            'judul_page' => $this->judul_page,
            'konten' => 'tindakan/tindakan_form',
            'judul_form' => 'Tambah '.$this->judul_page,
            'button' => 'Simpan',
            'action' => site_url('tindakan/create_action'),
	    'id_tindakan' => set_value('id_tindakan'),
	    'kode_tindakan' => set_value('kode_tindakan'),
	    'nama_tindakan' => set_value('nama_tindakan'),
	    'ksm' => set_value('ksm'),
	    'total_tindakan' => set_value('total_tindakan'),
	    'tarif_existing_rsgm' => set_value('tarif_existing_rsgm'),
	    'pembobotan_biaya_tidak_langsung' => set_value('pembobotan_biaya_tidak_langsung'),
	    'biaya_tidak_langsung_pertindakan' => set_value('biaya_tidak_langsung_pertindakan'),
	);
        $this->load->view('v_index', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'kode_tindakan' => $this->input->post('kode_tindakan',TRUE),
		'nama_tindakan' => $this->input->post('nama_tindakan',TRUE),
		'ksm' => $this->input->post('ksm',TRUE),
		'total_tindakan' => $this->input->post('total_tindakan',TRUE),
		'tarif_existing_rsgm' => $this->input->post('tarif_existing_rsgm',TRUE),
		'pembobotan_biaya_tidak_langsung' => $this->input->post('pembobotan_biaya_tidak_langsung',TRUE),
		'biaya_tidak_langsung_pertindakan' => $this->input->post('biaya_tidak_langsung_pertindakan',TRUE),
	    );

            $this->Tindakan_model->insert($data);
            $this->session->set_flashdata('message', message('success','Data berhasil disimpan'));
            redirect(site_url('tindakan'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Tindakan_model->get_by_id($id);

        if ($row) {
            $data = array(
                'judul_page' => $this->judul_page,
                'konten' => 'tindakan/tindakan_form',
                'judul_form' => 'Ubah '.$this->judul_page,
                'button' => 'Update',
                'action' => site_url('tindakan/update_action'),
		'id_tindakan' => set_value('id_tindakan', $row->id_tindakan),
		'kode_tindakan' => set_value('kode_tindakan', $row->kode_tindakan),
		'nama_tindakan' => set_value('nama_tindakan', $row->nama_tindakan),
		'ksm' => set_value('ksm', $row->ksm),
		'total_tindakan' => set_value('total_tindakan', $row->total_tindakan),
		'tarif_existing_rsgm' => set_value('tarif_existing_rsgm', $row->tarif_existing_rsgm),
		'pembobotan_biaya_tidak_langsung' => set_value('pembobotan_biaya_tidak_langsung', $row->pembobotan_biaya_tidak_langsung),
		'biaya_tidak_langsung_pertindakan' => set_value('biaya_tidak_langsung_pertindakan', $row->biaya_tidak_langsung_pertindakan),
	    );
            $this->load->view('v_index', $data);
        } else {
            $this->session->set_flashdata('message', message('danger','Data tidak ditemukan'));
            redirect(site_url('tindakan'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_tindakan', TRUE));
        } else {
            $total_tindakan_post = $this->input->post('total_tindakan',TRUE);
            $data = array(
		'kode_tindakan' => $this->input->post('kode_tindakan',TRUE),
		'nama_tindakan' => $this->input->post('nama_tindakan',TRUE),
		'ksm' => $this->input->post('ksm',TRUE),
		'total_tindakan' => $total_tindakan_post,
		'tarif_existing_rsgm' => $this->input->post('tarif_existing_rsgm',TRUE),
		'pembobotan_biaya_tidak_langsung' => $this->input->post('pembobotan_biaya_tidak_langsung',TRUE),
		'biaya_tidak_langsung_pertindakan' => $this->input->post('biaya_tidak_langsung_pertindakan',TRUE),
	    );
            $total_tindakan_old = get_data('tindakan','id_tindakan',$this->input->post('id_tindakan', TRUE), 'total_tindakan');
            if ($total_tindakan_old == $total_tindakan_post) {
                $this->Tindakan_model->update($this->input->post('id_tindakan', TRUE), $data);
                $this->session->set_flashdata('message', message('success','Data berhasil diupdate'));
                redirect('tindakan');
            } else {
                $this->Tindakan_model->update($this->input->post('id_tindakan', TRUE), $data);
                $this->session->set_flashdata('message', message('success','Data berhasil diupdate'));
                ?>
                <!-- <meta http-equiv="refresh" content="0; url=<?php echo base_url('tindakan/hitung_tindakan') ?>" /> -->
                <script type="text/javascript">
                    window.location="<?php echo base_url('tindakan/hitung_tindakan') ?>";
                </script>
                <?php 
            }
            
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Tindakan_model->get_by_id($id);

        if ($row) {
            $this->Tindakan_model->delete($id);
            $this->session->set_flashdata('message', message('success','Data berhasil dihapus'));
            ?>
                <meta http-equiv="refresh" content="0; url=<?php echo base_url('tindakan/hitung_tindakan') ?>" />
            <?php 
        } else {
            $this->session->set_flashdata('message', message('danger','Data tidak ditemukan'));
            redirect(site_url('tindakan'));
        }
    }

    public function import_excel()
    {
        // Fungsi untuk melakukan proses upload file
        $return = array();
        $this->load->library('upload'); // Load librari upload

        $config['upload_path'] = './files/excel/';
        $config['allowed_types'] = 'xlsx';
        $config['max_size'] = '2048';
        $config['overwrite'] = true;
        $config['file_name'] = 'import_tindakan';

        $this->upload->initialize($config); // Load konfigurasi uploadnya
        if($this->upload->do_upload('file_excel')){ // Lakukan upload dan Cek jika proses upload berhasil
            // Jika berhasil :
            $return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
        }else{
            // Jika gagal :
            $return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
            $this->session->set_flashdata('message',message($return['error'],'danger'));
            redirect('tindakan','refresh');
        }

		include APPPATH.'third_party/PHPExcel/PHPExcel.php';

		$filename = 'import_tindakan.xlsx';
					
		$excelreader = new PHPExcel_Reader_Excel2007();
		$loadexcel = $excelreader->load('files/excel/'.$filename.''); // Load file yang tadi diupload ke folder excel
		$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);
		
        //skip untuk header
		unset($sheet[1]);

        $this->db->trans_begin();

		foreach ($sheet as $rw) {
			if (empty($rw['A'])) {
				// code...
			} else {
				$data = array(
		'kode_tindakan' => $rw['A'],
		'nama_tindakan' => $rw['B'],
		'ksm' => $rw['C'],
		'total_tindakan' => $rw['D'],
		'tarif_existing_rsgm' => $rw['E'],
		'pembobotan_biaya_tidak_langsung' => $rw['F'],
		'biaya_tidak_langsung_pertindakan' => $rw['G'],
	);

				$this->db->insert('tindakan', $data);
			}
			
		}

		if ($this->db->trans_status() === FALSE)
		{
	        $this->db->trans_rollback();
	        $this->session->set_flashdata('message', message('gagal server,silahkan ulangi','error'));
			redirect('tindakan','refresh');
		}
		else
		{
	        $this->db->trans_commit();
	        $this->session->set_flashdata('message', message('data berhasil diimport','success'));
			redirect('tindakan/hitung_tindakan','refresh');
		}

    }

    public function _rules() 
    {
	$this->form_validation->set_rules('kode_tindakan', 'kode tindakan', 'trim|required');
	$this->form_validation->set_rules('nama_tindakan', 'nama tindakan', 'trim|required');
	$this->form_validation->set_rules('ksm', 'ksm', 'trim|required');
	$this->form_validation->set_rules('total_tindakan', 'total tindakan', 'trim|required');
	$this->form_validation->set_rules('tarif_existing_rsgm', 'tarif existing rsgm', 'trim|required');
	$this->form_validation->set_rules('pembobotan_biaya_tidak_langsung', 'pembobotan biaya tidak langsung', 'trim|required');
	$this->form_validation->set_rules('biaya_tidak_langsung_pertindakan', 'biaya tidak langsung pertindakan', 'trim|required');

	$this->form_validation->set_rules('id_tindakan', 'id_tindakan', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Tindakan.php */
/* Location: ./application/controllers/Tindakan.php */
/* Please DO NOT modify this information : */
/* Generated by Boy Kurniawan 2022-02-06 05:00:15 */
/* https://jualkoding.com */