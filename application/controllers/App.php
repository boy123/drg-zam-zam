<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class App extends CI_Controller {


     public function index()
     {
          if ($this->session->userdata('level') == '') {
               redirect('login');
          }
          $data = array(
               'judul_page' => "Dashboard",
               'konten' => 'home/view',
          );
          $this->load->view('v_index', $data);
     }

     public function reset_data($table)
     {
          $this->db->truncate($table);
          $this->session->set_flashdata('pesan', alert_biasa('Berhasil di reset','success'));
          redirect($table);
     }

     public function dev()
     {
          $this->session->set_flashdata('pesan', alert_biasa('Sedang dalam pengembangan','info'));
          redirect("app");
     }

     public function getTindakan()
     {
          $ksm = $this->input->get('ksm');
          echo '<option value="">pilih tindakan</option>';
          $this->db->where('ksm', $ksm);
          foreach ($this->db->get('tindakan')->result()
               as $key => $value){
               ?>
               <option value="<?php echo $value->kode_tindakan ?>"><?php echo $value->kode_tindakan.' | '.$value->nama_tindakan ?></option>
          <?php 
          
          } 
          
     }

     public function export($export)
     {
          $this->load->view('export/'.$export);
     }

     public function cetak_pdf($kode_tindakan)
     {
          // $this->load->library('mpdf_l');
          // $mpdf                           = $this->mpdf_l->load();
          // $mpdf->allow_charset_conversion = true;  // Set by default to TRUE
          // $mpdf->charset_in               = 'UTF-8';
          // $mpdf->autoLangToFont           = true;
          // $mpdf->AddPage('L'); // P - L
          // $data['kode_tindakan'] = $kode_tindakan;
          // $html = $this->load->view('cetak/tindakan_lap',$data, TRUE);
          

          // $mpdf->WriteHTML($html);

          // $output = $kode_tindakan.'.pdf';
          // $mpdf->Output("$output", 'I');

          $data['kode_tindakan'] = $kode_tindakan;
          $data['action'] = 'pdf';
          $this->load->view('cetak/tindakan_lap', $data);

     }

     public function loadTindakan($kode_tindakan)
     {
          $data['kode_tindakan'] = $kode_tindakan;
          $this->load->view('cetak/tindakan_lap', $data);
     }

     public function cetak_excel($kode_tindakan)
     {
          $data['kode_tindakan'] = $kode_tindakan;
          $this->load->view('cetak/tindakan_excel', $data);
          
     }

     public function cetak_view()
     {
          if ($this->session->userdata('level') == '') {
               redirect('login');
          }
          $data = array(
               'judul_page' => "Cetak Tindakan",
               'konten' => 'cetak/view',
          );
          $this->load->view('v_index', $data);
     }

     public function rekapitulasi()
     {
          if ($this->session->userdata('level') == '') {
               redirect('login');
          }
          $data = array(
               'judul_page' => "Rekapitulasi",
               'konten' => 'cetak/rekapitulasi',
          );
          $this->load->view('v_index', $data);
     }

}

/* End of file App.php */
