<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Alat extends CI_Controller
{
    var $judul_page = 'Data Alat';
    function __construct()
    {
        parent::__construct();
        $this->load->model('Alat_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $alat = $this->Alat_model->get_all();

        $data = array(
            'alat_data' => $alat,
            'judul_page' => $this->judul_page,
            'konten' => 'alat/alat_list',
        );
        $this->load->view('v_index', $data);
    }

    

    public function create() 
    {
        $data = array(
            'judul_page' => $this->judul_page,
            'konten' => 'alat/alat_form',
            'judul_form' => 'Tambah '.$this->judul_page,
            'button' => 'Simpan',
            'action' => site_url('alat/create_action'),
	    'id_alat' => set_value('id_alat'),
	    'nama_alat' => set_value('nama_alat'),
	    'volume' => set_value('volume'),
	    'satuan' => set_value('satuan'),
	    'merk' => set_value('merk'),
	    'harga' => set_value('harga'),
	    'link_produk' => set_value('link_produk'),
	);
        $this->load->view('v_index', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'nama_alat' => $this->input->post('nama_alat',TRUE),
		'volume' => $this->input->post('volume',TRUE),
		'satuan' => $this->input->post('satuan',TRUE),
		'merk' => $this->input->post('merk',TRUE),
		'harga' => $this->input->post('harga',TRUE),
		'link_produk' => $this->input->post('link_produk',TRUE),
	    );

            $this->Alat_model->insert($data);
            $this->session->set_flashdata('message', message('success','Data berhasil disimpan'));
            redirect(site_url('alat'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Alat_model->get_by_id($id);

        if ($row) {
            $data = array(
                'judul_page' => $this->judul_page,
                'konten' => 'alat/alat_form',
                'judul_form' => 'Ubah '.$this->judul_page,
                'button' => 'Update',
                'action' => site_url('alat/update_action'),
		'id_alat' => set_value('id_alat', $row->id_alat),
		'nama_alat' => set_value('nama_alat', $row->nama_alat),
		'volume' => set_value('volume', $row->volume),
		'satuan' => set_value('satuan', $row->satuan),
		'merk' => set_value('merk', $row->merk),
		'harga' => set_value('harga', $row->harga),
		'link_produk' => set_value('link_produk', $row->link_produk),
	    );
            $this->load->view('v_index', $data);
        } else {
            $this->session->set_flashdata('message', message('danger','Data tidak ditemukan'));
            redirect(site_url('alat'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_alat', TRUE));
        } else {
            $data = array(
		'nama_alat' => $this->input->post('nama_alat',TRUE),
		'volume' => $this->input->post('volume',TRUE),
		'satuan' => $this->input->post('satuan',TRUE),
		'merk' => $this->input->post('merk',TRUE),
		'harga' => $this->input->post('harga',TRUE),
		'link_produk' => $this->input->post('link_produk',TRUE),
	    );

            $this->db->where('id_alat', $this->input->post('id_alat'));
            $this->db->update('alat_tindakan', array(
                'volume' => $this->input->post('volume',TRUE),
                'satuan' => $this->input->post('satuan',TRUE),
                'merk' => $this->input->post('merk',TRUE),
                'harga' => $this->input->post('harga',TRUE),
                'link_produk' => $this->input->post('link_produk',TRUE),
            ));

            $this->Alat_model->update($this->input->post('id_alat', TRUE), $data);
            $this->session->set_flashdata('message', message('success','Data berhasil diupdate'));
            redirect(site_url('alat'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Alat_model->get_by_id($id);

        if ($row) {
            $this->Alat_model->delete($id);
            $this->session->set_flashdata('message', message('success','Data berhasil dihapus'));
            redirect(site_url('alat'));
        } else {
            $this->session->set_flashdata('message', message('danger','Data tidak ditemukan'));
            redirect(site_url('alat'));
        }
    }

    public function import_excel()
    {
        // Fungsi untuk melakukan proses upload file
        $return = array();
        $this->load->library('upload'); // Load librari upload

        $config['upload_path'] = './files/excel/';
        $config['allowed_types'] = 'xlsx';
        $config['max_size'] = '2048';
        $config['overwrite'] = true;
        $config['file_name'] = 'import_alat';

        $this->upload->initialize($config); // Load konfigurasi uploadnya
        if($this->upload->do_upload('file_excel')){ // Lakukan upload dan Cek jika proses upload berhasil
            // Jika berhasil :
            $return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
        }else{
            // Jika gagal :
            $return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());

            $this->session->set_flashdata('message',message($return['error'],'error'));
            redirect('alat','refresh');
        }

		include APPPATH.'third_party/PHPExcel/PHPExcel.php';

		$filename = 'import_alat.xlsx';
					
		$excelreader = new PHPExcel_Reader_Excel2007();
		$loadexcel = $excelreader->load('files/excel/'.$filename.''); // Load file yang tadi diupload ke folder excel
		$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);
		
        //skip untuk header
		unset($sheet[1]);

        $this->db->trans_begin();

		foreach ($sheet as $rw) {
			if (empty($rw['A'])) {
				// code...
			} else {
				$data = array(
		'nama_alat' => $rw['A'],
		'volume' => $rw['B'],
		'satuan' => $rw['C'],
		'merk' => $rw['D'],
		'harga' => $rw['E'],
		'link_produk' => $rw['F'],
	);

				$this->db->insert('alat', $data);
			}
			
		}

		if ($this->db->trans_status() === FALSE)
		{
	        $this->db->trans_rollback();
	        $this->session->set_flashdata('message', message('gagal server,silahkan ulangi','error'));
			redirect('alat','refresh');
		}
		else
		{
	        $this->db->trans_commit();
	        $this->session->set_flashdata('message', message('data berhasil diimport','success'));
			redirect('alat','refresh');
		}

    }

    public function _rules() 
    {
	$this->form_validation->set_rules('nama_alat', 'nama alat', 'trim|required');
	$this->form_validation->set_rules('volume', 'volume', 'trim|required');
	$this->form_validation->set_rules('satuan', 'satuan', 'trim|required');
	$this->form_validation->set_rules('merk', 'merk', 'trim|required');
	$this->form_validation->set_rules('harga', 'harga', 'trim|required');
	$this->form_validation->set_rules('link_produk', 'link produk', 'trim|required');

	$this->form_validation->set_rules('id_alat', 'id_alat', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Alat.php */
/* Location: ./application/controllers/Alat.php */
/* Please DO NOT modify this information : */
/* Generated by Boy Kurniawan 2022-02-08 02:18:21 */
/* https://jualkoding.com */