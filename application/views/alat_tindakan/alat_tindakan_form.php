<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
     <div class="card border-top border-0 border-4 border-info">
          <div class="card-body">
               <div class="border p-4 rounded">
                    <div class="card-title d-flex align-items-center">
                         <div><i class="bx bx-edit-alt me-1 font-22 text-info"></i>
                         </div>
                         <h5 class="mb-0 text-info"><?php echo $judul_form ?></h5>
                    </div>
                    <hr />

                    <div class="row mb-3">
                         <label for="kode_tindakan" class="col-sm-3
                         col-form-label">Kode Tindakan
                              <?php echo form_error('kode_tindakan') ?></label>
                         <div class="col-sm-9">
                              <select name="kode_tindakan" id="kode_tindakan" class="single-select form-control " required>
                                   <option value="<?php echo $kode_tindakan ?>"><?php echo $kode_tindakan ?></option>
                                   <?php foreach ($this->db->get('tindakan')->result()
                                    as $key => $value): ?>
                                   <option value="<?php echo $value->kode_tindakan ?>"><?php echo $value->kode_tindakan.' | '.$value->nama_tindakan ?></option>
                                   <?php endforeach ?>
                              </select>
                         </div>
                    </div>
                    <div class="row mb-3">
                         <label for="nama_alat" class="col-sm-3
                         col-form-label">Nama Alat </label>
                         <div class="col-sm-9">
                              <select name="id_alat" id="id_alat" class="single-select form-control" onchange="getAlat()" required>
                                   <option value="<?php echo $id_alat ?>"><?php echo $id_alat ?></option>
                                   <?php foreach ($this->db->get('alat')->result() as $key => $value): ?>
                                   <option value="<?php echo $value->id_alat ?>"><?php echo $value->nama_alat ?></option>
                                   <?php endforeach ?>
                              </select>
                              <input type="hidden" value="<?php echo $nama_alat ?>" id="nama_alat" required>
                         </div>
                    </div>
                    <div class="row mb-3">
                         <label for="volume" class="col-sm-3
                         col-form-label">Volume
                              <?php echo form_error('volume') ?></label>
                         <div class="col-sm-9">
                              <input type="text" class="form-control" name="volume" id="volume" placeholder="Volume"
                                   value="<?php echo $volume; ?>" />
                         </div>
                    </div>
                    <div class="row mb-3">
                         <label for="satuan" class="col-sm-3
                         col-form-label">Satuan
                              <?php echo form_error('satuan') ?></label>
                         <div class="col-sm-9">
                              <input type="text" class="form-control" name="satuan" id="satuan" placeholder="Satuan"
                                   value="<?php echo $satuan; ?>" />
                         </div>
                    </div>
                    <div class="row mb-3">
                         <label for="umur_ekonomis" class="col-sm-3
                         col-form-label">Umur Ekonomis
                              <?php echo form_error('umur_ekonomis') ?></label>
                         <div class="col-sm-9">
                              <input type="text" class="form-control" onkeyup="hitung()" name="umur_ekonomis" id="umur_ekonomis"
                                   placeholder="Umur Ekonomis" value="<?php echo $umur_ekonomis; ?>" />
                         </div>
                    </div>
                    <div class="row mb-3">
                         <label for="jumlah_pemakaian" class="col-sm-3
                         col-form-label">Jumlah Pemakaian
                              <?php echo form_error('jumlah_pemakaian') ?></label>
                         <div class="col-sm-9">
                              <input type="text" class="form-control" onkeyup="hitung()" name="jumlah_pemakaian" id="jumlah_pemakaian"
                                   placeholder="Jumlah Pemakaian" value="<?php echo $jumlah_pemakaian; ?>" />
                         </div>
                    </div>
                    <div class="row mb-3">
                         <label for="merk" class="col-sm-3
                         col-form-label">Merk
                              <?php echo form_error('merk') ?></label>
                         <div class="col-sm-9">
                              <input type="text" class="form-control" name="merk" id="merk" placeholder="Merk"
                                   value="<?php echo $merk; ?>" />
                         </div>
                    </div>
                    <div class="row mb-3">
                         <label for="harga" class="col-sm-3
                         col-form-label">Harga
                              <?php echo form_error('harga') ?></label>
                         <div class="col-sm-9">
                              <input type="text" class="form-control" onkeyup="hitung()" name="harga" id="harga" placeholder="Harga"
                                   value="<?php echo $harga; ?>" />
                         </div>
                    </div>
                    <div class="row mb-3">
                         <label for="link_produk" class="col-sm-3
                         col-form-label">Link Produk
                              <?php echo form_error('link_produk') ?></label>
                         <div class="col-sm-9">
                              <textarea class="form-control" rows="3" name="link_produk" id="link_produk" rows="3"
                                   placeholder="Link Produk"><?php echo $link_produk; ?></textarea>
                         </div>
                    </div>
                    <div class="row mb-3">
                         <label for="harga_perpemakaian" class="col-sm-3
                         col-form-label">Harga Perpemakaian
                              <?php echo form_error('harga_perpemakaian') ?></label>
                         <div class="col-sm-9">
                              <input type="text" class="form-control" name="harga_perpemakaian" id="harga_perpemakaian"
                                   placeholder="Harga Perpemakaian" value="<?php echo $harga_perpemakaian; ?>" />
                         </div>
                    </div>
                    <input type="hidden" name="id_alat_tindakan" value="<?php echo $id_alat_tindakan; ?>" />
                    <button type="submit" class="btn btn-primary"><i class="bx bx-save"></i>
                         <?php echo $button ?></button>
                    <a href="<?php echo site_url('alat_tindakan') ?>" class="btn btn-outline-info"><i
                              class="bx bx-exit"></i> Cancel</a>

               </div>
          </div>
     </div>
</form>

<script>
     var total_tindakan = '<?php echo total_tindakan() ?>';
     function hitung(){

          var hasil = parseInt($("#harga").val()) / parseInt($("#umur_ekonomis").val()) * parseInt($("#jumlah_pemakaian").val()) / parseInt(total_tindakan);
          $("#harga_perpemakaian").val(hasil);
         
     }

     function getAlat() {
          var id = $("#id_alat").val();
          $.ajax({
               type: "GET",
               url: "alat_tindakan/get_alat/"+id,
               dataType: "json",
               success: function (response) {
                    $("#nama_alat").val(response[0].nama_alat);
                    $("#volume").val(response[0].volume);
                    $("#satuan").val(response[0].satuan);
                    $("#merk").val(response[0].merk);
                    $("#harga").val(response[0].harga);
                    $("#link_produk").val(response[0].link_produk);
               }
          });
     }
</script>