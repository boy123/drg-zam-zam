<?php 
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=$kode_tindakan.xls");
?>
<style>
         .text-center {
               text-align: center;
          }
          .text-right {
               text-align: right;
          }

          .table {
               width: 100%;
               max-width: 100%;
               border-collapse: collapse;
               /*border-collapse: separate;*/
          }

          .table-bordered tr td {
               border: 1px solid black;
               padding-left: 6px;
               padding-right: 6px;
          }

          body {
               font-family: "Arial";
               font-size: 8pt;
          }

          .border-bottom {
               border-bottom: 1px solid black;
          }

          .bold {
               font-weight: bold;
          }
          .bg{
               background-color: yellow;
          }

          .mt{
               margin-top: 20px;
          }
    </style>
     <?php 
     
     $header = $this->db->get_where('tindakan',['kode_tindakan'=>$kode_tindakan])->row();
     // $alokasi_btl = total_btl() * ($header->pembobotan_biaya_tidak_langsung) / $header->total_tindakan;
     $alokasi_btl = $header->biaya_tidak_langsung_pertindakan;
     $total_bsp = 0;
     ?>
     <table class="table table-bordered">
          <tr>
               <td class="bold bg">KSM</td>
               <td><?php echo $header->ksm ?></td>
          </tr>
          <tr>
               <td class="bold bg">NAMA TINDAKAN</td>
               <td><?php echo $header->nama_tindakan ?></td>
          </tr>
          <tr>
               <td class="bold bg">KODE TINDAKAN</td>
               <td><?php echo $header->kode_tindakan ?></td>
          </tr>
          <tr>
               <td class="bold bg">JUMLAH TINDAKAN NORMATIF PER TAHUN</td>
               <td><?php echo $header->total_tindakan ?></td>
          </tr>
          <tr>
               <td class="bold bg">DASAR PENGALOKASIAN BIAYA TIDAK LANGSUNG</td>
               <td><?php echo $header->pembobotan_biaya_tidak_langsung ?> %</td>
          </tr>
     </table>

     <table class="table table-bordered mt">
          <tr>
               <td class="bold" colspan="9">FORMULARIUM BAHAN</td>
          </tr>
          <tr>
               <td class="bold bg">NAMA BHP</td>
               <td class="bold bg">VOLUME</td>
               <td class="bold bg">SATUAN</td>
               <td class="bold bg">SATUAN PEMAKAIAN</td>
               <td class="bold bg">JUMLAH PEMAKAIAN</td>
               <td class="bold bg">MERK</td>
               <td class="bold bg">HARGA</td>
               <td class="bold bg">LINK PRODUK</td>
               <td class="bold bg">HARGA PER PEMAKAIAN</td>
          </tr>

          <?php 

          $r_bahan = $this->db->get_where('bahan_tindakan', ['kode_tindakan'=>$header->kode_tindakan]);
          if ($r_bahan->num_rows() == 0) {
               echo "<tr>
                         <td class=\"bold text-center\" colspan=\"9\">TIDAK ADA DATA</td>
                    </tr>";
          } else {

               $total_bahan = 0;
               foreach($r_bahan->result() as $rw ): ?>
          <tr>
               <td><?php echo $rw->nama_bahan ?></td>
               <td><?php echo $rw->volume ?></td>
               <td><?php echo $rw->satuan ?></td>
               <td><?php echo $rw->satuan_pemakaian ?></td>
               <td><?php echo $rw->jumlah_pemakaian ?></td>
               <td><?php echo $rw->merk ?></td>
               <td><?php echo number_format($rw->harga,2,',','.') ?></td>
               <td><?php echo $rw->link_produk ?></td>
               <td class="text-right"><?php echo number_format($rw->harga_perpemakaian,2,',','.'); $total_bahan = $total_bahan + $rw->harga_perpemakaian ?></td>
          </tr>
          <?php endforeach ?>
          <tr>
               <td class="bold bg text-right" colspan="8">TOTAL BIAYA PERPEMAKAIAN</td>
               <td class="text-right">Rp. <?php echo number_format($total_bahan,2,',','.') ?></td>
          </tr>

          <?php } ?>


     </table>

     <table class="table table-bordered mt">
          <tr>
               <td class="bold" colspan="9">FORMULARIUM ALAT</td>
          </tr>
          <tr>
               <td class="bold bg">NAMA ALAT</td>
               <td class="bold bg">VOLUME</td>
               <td class="bold bg">SATUAN</td>
               <td class="bold bg">UMUR EKONOMIS</td>
               <td class="bold bg">JUMLAH PEMAKAIAN</td>
               <td class="bold bg">MERK</td>
               <td class="bold bg">HARGA</td>
               <td class="bold bg">LINK PRODUK</td>
               <td class="bold bg">HARGA PER PEMAKAIAN</td>
          </tr>

          <?php 

          $r_alat = $this->db->get_where('alat_tindakan', ['kode_tindakan'=>$header->kode_tindakan]);
          if ($r_alat->num_rows() == 0) {
               echo "<tr>
                         <td class=\"bold text-center\" colspan=\"9\">TIDAK ADA DATA</td>
                    </tr>";
          } else {

               $total_alat = 0;
               foreach($r_alat->result() as $rw ): ?>
          <tr>
               <td><?php echo $rw->nama_alat ?></td>
               <td><?php echo $rw->volume ?></td>
               <td><?php echo $rw->satuan ?></td>
               <td><?php echo $rw->umur_ekonomis ?></td>
               <td><?php echo $rw->jumlah_pemakaian ?></td>
               <td><?php echo $rw->merk ?></td>
               <td><?php echo number_format($rw->harga,2,',','.') ?></td>
               <td><?php echo $rw->link_produk ?></td>
               <td class="text-right"><?php echo number_format($rw->harga_perpemakaian,2,',','.'); $total_alat = $total_alat + $rw->harga_perpemakaian ?></td>
          </tr>
          <?php endforeach ?>
          <tr>
               <td class="bold bg text-right" colspan="8">TOTAL BIAYA PERPEMAKAIAN</td>
               <td class="text-right">Rp. <?php echo number_format($total_alat,2,',','.') ?></td>
          </tr>
          <tr>
               <td class="bold bg text-right" colspan="8">ALOKASI BIAYA TIDAK LANGSUNG</td>
               <td class="text-right">Rp. <?php echo number_format($alokasi_btl,2,',','.') ?></td>
          </tr>
          <tr>
               <td class="bold bg text-right" colspan="8">TOTAL BIAYA SATUAN PER TINDAKAN</td>
               <td class="text-right">Rp. <?php 
               $total_bsp = $total_bahan + $total_alat + $alokasi_btl;
               echo number_format($total_bsp,2,',','.') ?></td>
          </tr>

          <?php } ?>


     </table>

