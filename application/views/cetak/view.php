<form>
     <div class="card border-top border-0 border-4 border-info">
          <div class="card-body">
               <div class="border p-4 rounded">
                    <div class="card-title d-flex align-items-center">
                         <div><i class="bx bx-edit-alt me-1 font-22 text-info"></i>
                         </div>
                         <h5 class="mb-0 text-info">Cetak Tindakan</h5>
                    </div>
                    <hr />

                    <div class="row mb-3">
                         <label for="nama_bahan" class="col-sm-3
                         col-form-label">KSM </label>
                         <div class="col-sm-9">
                              <select name="ksm" id="ksm" onchange="getKsm()" class="single-select form-control" required>
                                   <option value="">pilih ksm</option>
                                   <?php foreach ($this->db->get('ksm')->result()
                                    as $key => $value): ?>
                                   <option value="<?php echo $value->ksm ?>"><?php echo $value->ksm ?></option>
                                   <?php endforeach ?>
                              </select>
                         </div>
                    </div>

                    <div class="row mb-3">
                         <label for="nama_bahan" class="col-sm-3
                         col-form-label">Tindakan </label>
                         <div class="col-sm-9">
                              <select name="kode_tindakan" id="kode_tindakan" onchange="gettindakan()" class="single-select form-control " required>
                                   <option value="">pilih tindakan</option>
                              </select>
                         </div>
                    </div>

                    <div class="row mb-3">
                         <div class="col-md-12" id="view">

                         </div>
                    </div>

                    <a id="excel" class="btn btn-primary">EXCEL</a>
                    <a id="pdf" class="btn btn-info">PDF</a>
                    

               </div>
          </div>
     </div>
</form>

<script>

     function getKsm() { 
          var id = $("#ksm").val();
          $.ajax({
               type: "GET",
               url: "app/getTindakan?ksm="+id,
               dataType: "html",
               success: function (response) {
                    $("#kode_tindakan").html(response);
               }
          });
      }

     function gettindakan() {
          var id = $("#kode_tindakan").val();

          $.ajax({
               type: "GET",
               url: "app/loadTindakan/"+id,
               dataType: "html",
               success: function (response) {
                    $("#view").html(response);

                    $("#excel").attr("href", "<?php echo base_url()?>app/cetak_excel/"+id);
          
                    $("#pdf").attr("href", "<?php echo base_url()?>app/cetak_pdf/"+id);
                    $("#pdf").attr("target", "_blank");
               }
          });          

          
     }
</script>