<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Hitung Tindakan</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  </head>
  <body>
    <div class="container">
      <h2>Progress Hitung Tindakan</h2>
      <div class="progress">
        <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%"> 0% Progress </div>
      </div>
      <center>
        <h5 id="redirect" style="display: none;">sedang mengalihkan halaman..</h5>
      </center>
    </div>
    <script type="text/javascript">
      var persen = 0;
      var iterasi = 0;
      var akum_total_tindakan = "<?php echo total_tindakan() ?>";
      var total_btl = "<?php echo total_btl() ?>";
      var result = null;
      var interval = null;
      var label = "";
      $(document).ready(function() {
        $.ajax({
          url: '<?php echo base_url() ?>tindakan/get_tindakan',
          type: 'GET',
          dataType: 'json'
        }).done(function(rsl) {
          console.log("success");
        }).fail(function() {
          console.log("error");
        }).always(function(rsl) {
          console.log("complete");
          result = rsl;
          proses();
        });
      });

      function sleepFor(sleepDuration) {
        var now = new Date().getTime();
        while (new Date().getTime() < now + sleepDuration) {
          /* Do nothing */
        }
      }

      function proses() {
        // interval = setInterval(varName, 100);
        for (var i = 0; i < result.length; i++) {
          var success = null;
          iterasi = i + 1;
          updateProgress(result.length, iterasi, result[i]['total_tindakan'], result[i]['id_tindakan']);
        }
        //batas for loop
        
      }

      function updateProgress(total, iterasi, total_tindakan, id_tindakan) {

        setTimeout(function() {
            $.ajax({
              url: ' <?php echo base_url() ?>tindakan/proses_hitung_tindakan/'+total_tindakan+'/'+id_tindakan+'/'+akum_total_tindakan+'/'+total_btl,
              type: 'GET',
              async: false,
              dataType: 'json'
            }).done(function() {
              // console.log("success updateProgress");
              persen = parseFloat((iterasi / total) * 100).toFixed(2);
              if (persen < 100) {
                label = persen + "% Progress";
              } else {
                label = persen + "% Complete";
              }
              console.log(label);
              var progressBar = $(".progress-bar");
              progressBar.css('width', persen + '%');
              progressBar.attr('aria-valuenow', persen);
              progressBar.text(label);
            }).fail(function() {
              console.log("error updateProgress");
            }).always(function() {
              console.log("complete updateProgress " + iterasi);

            });
            console.log(label);
            if (label == "100.00% Complete") {
              $("#redirect").show();
              interval = setInterval(function(){
                window.location="<?php echo base_url() ?>tindakan";
              },2000);
            }
        }, 1000);

        
      }
    </script>
  </body>
</html>