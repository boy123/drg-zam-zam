<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
     <div class="card border-top border-0 border-4 border-info">
          <div class="card-body">
               <div class="border p-4 rounded">
                    <div class="card-title d-flex align-items-center">
                         <div><i class="bx bx-edit-alt me-1 font-22 text-info"></i>
                         </div>
                         <h5 class="mb-0 text-info"><?php echo $judul_form ?></h5>
                    </div>
                    <hr />

                    <div class="row mb-3">
                         <label for="kode_tindakan" class="col-sm-3
                         col-form-label">Kode Tindakan
                              <?php echo form_error('kode_tindakan') ?></label>
                         <div class="col-sm-9">
                              <input type="text" class="form-control" name="kode_tindakan" id="kode_tindakan"
                                   placeholder="Kode Tindakan" value="<?php echo $kode_tindakan; ?>" />
                         </div>
                    </div>
                    <div class="row mb-3">
                         <label for="nama_tindakan" class="col-sm-3
                         col-form-label">Nama Tindakan
                              <?php echo form_error('nama_tindakan') ?></label>
                         <div class="col-sm-9">
                              <input type="text" class="form-control" name="nama_tindakan" id="nama_tindakan"
                                   placeholder="Nama Tindakan" value="<?php echo $nama_tindakan; ?>" />
                         </div>
                    </div>
                    <div class="row mb-3">
                         <label for="ksm" class="col-sm-3
                         col-form-label">KSM
                              <?php echo form_error('ksm') ?></label>
                         <div class="col-sm-9">
                               <select name="ksm" id="ksm" class="single-select form-control " required>
                                   <option value="<?php echo $ksm ?>"><?php echo $ksm ?></option>
                                   <?php foreach ($this->db->get('ksm')->result()
                                    as $key => $value): ?>
                                   <option value="<?php echo $value->ksm ?>"><?php echo $value->ksm ?></option>
                                   <?php endforeach ?>
                              </select>
                         </div>
                    </div>
                    <div class="row mb-3">
                         <label for="total_tindakan" class="col-sm-3
                         col-form-label">Total Tindakan
                              <?php echo form_error('total_tindakan') ?></label>
                         <div class="col-sm-9">
                              <input type="text" class="form-control" onkeyup="totalBtl()" name="total_tindakan" id="total_tindakan"
                                   placeholder="Total Tindakan" value="<?php echo $total_tindakan; ?>" autocomplete="off"/>
                         </div>
                    </div>
                    <div class="row mb-3">
                         <label for="tarif_existing_rsgm" class="col-sm-3
                         col-form-label">Tarif Existing
                              <?php echo form_error('tarif_existing_rsgm') ?></label>
                         <div class="col-sm-9">
                              <input type="text" class="form-control" name="tarif_existing_rsgm"
                                   id="tarif_existing_rsgm" placeholder="Tarif Existing Rsgm"
                                   value="<?php echo $tarif_existing_rsgm; ?>" />
                         </div>
                    </div>
                    <div class="row mb-3">
                         <label for="pembobotan_biaya_tidak_langsung" class="col-sm-3
                         col-form-label">Pembobotan Biaya Tidak Langsung
                              <?php echo form_error('pembobotan_biaya_tidak_langsung') ?></label>
                         <div class="col-sm-9">
                              <input type="text" class="form-control" name="pembobotan_biaya_tidak_langsung"
                                   id="pembobotan_biaya_tidak_langsung" placeholder="Pembobotan Biaya Tidak Langsung"
                                   value="<?php echo $pembobotan_biaya_tidak_langsung; ?>" />
                         </div>
                    </div>
                    <div class="row mb-3">
                         <label for="biaya_tidak_langsung_pertindakan" class="col-sm-3
                         col-form-label">Biaya Tidak Langsung Pertindakan
                              <?php echo form_error('biaya_tidak_langsung_pertindakan') ?></label>
                         <div class="col-sm-9">
                              <input type="text" class="form-control" name="biaya_tidak_langsung_pertindakan"
                                   id="biaya_tidak_langsung_pertindakan" placeholder="Biaya Tidak Langsung Pertindakan"
                                   value="<?php echo $biaya_tidak_langsung_pertindakan; ?>" />
                         </div>
                    </div>
                    <input type="hidden" name="id_tindakan" value="<?php echo $id_tindakan; ?>" />
                    <button type="submit" class="btn btn-primary"><i class="bx bx-save"></i>
                         <?php echo $button ?></button>
                    <a href="<?php echo site_url('tindakan') ?>" class="btn btn-outline-info"><i class="bx bx-exit"></i>
                         Cancel</a>

               </div>
          </div>
     </div>
</form>

<script type="text/javascript">
     function totalBtl() {
          var total = '<?php echo total_tindakan() ?>';
          var hasil = $("#total_tindakan").val() / total;
          $("#pembobotan_biaya_tidak_langsung").val(hasil);
          console.log(hasil);
          totalBtlTindakan();
     }

     function totalBtlTindakan() {
          var total = '<?php echo total_btl() ?>';
          var hasil = ( $("#pembobotan_biaya_tidak_langsung").val() * total )  / $("#total_tindakan").val() ;
          $("#biaya_tidak_langsung_pertindakan").val(hasil);
     }

     // $(document).ready(function() {
     //      $("#total_tindakan").keypress(function() {
     //           var total = '<?php echo total_tindakan() ?>';
     //           var hasil = parseInt($("#total_tindakan").val()) / parseInt(total);
     //           $("#pembobotan_biaya_tidak_langsung").val(hasil);
     //      });
     // });
</script>