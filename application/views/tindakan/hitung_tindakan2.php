<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
	<title>Hitung Tindakan</title>
</head>
<body>

<div class="w3-container">
	<h2>Hitung Tindakan</h2>

	<div class="w3-light-grey">
	  <div id="myBar" class="w3-container w3-green w3-center" style="width:0%">0%</div>
	</div>
	<!-- <p><button class="w3-button w3-green" id="start">Mulai</button></p> -->
	<p id="myP">Menghitung <span id="count">0</span> dari <span id="totaldata"></span> data</p>
	<center>
		<h5 id="redirect" style="display: none;">sedang mengalihkan halaman..</h5>
	</center>
	<div id="box-error" style="margin-top: 10px; border: solid 1px red;">
	</div>
</div>


<script type="text/javascript">

	$(document).ready(function() {
		$("#box-error").hide();
		// $("#start").click(function() {
			getTindakan();
		// });

	});

	var persen = 0;
	var i = 0;
	var akum_total_tindakan = "<?php echo total_tindakan() ?>";
	var total_btl = "<?php echo total_btl() ?>";
	var result = null;
	var interval = null;
	var label = "";
	var iteration = 0;

	function getTindakan() {
		// $("#start").hide();
		$.ajax({
          url: '<?php echo base_url() ?>tindakan/get_tindakan',
          type: 'GET',
          dataType: 'json'
        }).done(function(rsl) {
          console.log("success");
        }).fail(function() {
          console.log("error");
        }).always(function(rsl) {
          console.log("complete");
          result = rsl;
          document.getElementById("totaldata").innerHTML = result.length;
          interval = setInterval(proses, 10);
        });
	}

	function proses() {
		if (persen >= 100) {
			console.log("stop");
			clearInterval(interval);
			document.getElementById("myP").className = "w3-text-green w3-animate-opacity";
      		document.getElementById("myP").innerHTML = "Sukses update "+result.length+" data!";
			if (label == "100.00% Complete") {
              $("#redirect").show();
              interval = setInterval(function(){
                window.location="<?php echo base_url() ?>tindakan";
              },2000);
            }
		} else {
			if ( i < result.length ) {
				updateProgress(result.length, i+1, result[i]['total_tindakan'], result[i]['id_tindakan']);
				i++;
			}
		}
	}

	function updateProgress(total, iterasi, total_tindakan, id_tindakan) {
        $.ajax({
          url: ' <?php echo base_url() ?>tindakan/proses_hitung_tindakan/'+total_tindakan+'/'+id_tindakan+'/'+akum_total_tindakan+'/'+total_btl,
          type: 'GET',
          // async: false,
          dataType: 'json'
        }).done(function() {
          // console.log("success updateProgress");
          iteration++;
          persen = parseFloat((iteration / total) * 100).toFixed(2);
          if (persen < 100) {
            label = persen + "% Progress";
          } else {
            label = persen + "% Complete";
          }
          var progressBar = $("#myBar");
          progressBar.css('width', persen + '%');
          progressBar.text(label);
          document.getElementById("count").innerHTML = iteration;
        }).fail(function() {
          console.log("error updateProgress");
          $("#box-error").append('<li style="color: red"><b>gagal hitung di data ke '+iteration+'</b></li>');
        }).always(function() {
          console.log("complete updateProgress " + iteration);

        });
        console.log(label);
      }

</script>


</body>
</html>