<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
	 <div class="card border-top border-0 border-4 border-info">
     <div class="card-body">
          <div class="border p-4 rounded">
               <div class="card-title d-flex align-items-center">
                    <div><i class="bx bx-edit-alt me-1 font-22 text-info"></i>
                    </div>
                    <h5 class="mb-0 text-info"><?php echo $judul_form ?></h5>
               </div>
               <hr />
               
	 <div class="row mb-3">
                    <label for="nama_alat" class="col-sm-3
                         col-form-label">Nama Alat
                         <?php echo form_error('nama_alat') ?></label>
                    <div class="col-sm-9">
                         <input type="text" class="form-control" name="nama_alat"
                              id="nama_alat" placeholder="Nama Alat"
                              value="<?php echo $nama_alat; ?>" />
                    </div>
               </div>
	 <div class="row mb-3">
                    <label for="volume" class="col-sm-3
                         col-form-label">Volume
                         <?php echo form_error('volume') ?></label>
                    <div class="col-sm-9">
                         <input type="text" class="form-control" name="volume"
                              id="volume" placeholder="Volume"
                              value="<?php echo $volume; ?>" />
                    </div>
               </div>
	 <div class="row mb-3">
                    <label for="satuan" class="col-sm-3
                         col-form-label">Satuan
                         <?php echo form_error('satuan') ?></label>
                    <div class="col-sm-9">
                         <input type="text" class="form-control" name="satuan"
                              id="satuan" placeholder="Satuan"
                              value="<?php echo $satuan; ?>" />
                    </div>
               </div>
	 <div class="row mb-3">
                    <label for="merk" class="col-sm-3
                         col-form-label">Merk
                         <?php echo form_error('merk') ?></label>
                    <div class="col-sm-9">
                         <input type="text" class="form-control" name="merk"
                              id="merk" placeholder="Merk"
                              value="<?php echo $merk; ?>" />
                    </div>
               </div>
	 <div class="row mb-3">
                    <label for="harga" class="col-sm-3
                         col-form-label">Harga
                         <?php echo form_error('harga') ?></label>
                    <div class="col-sm-9">
                         <input type="text" class="form-control" name="harga"
                              id="harga" placeholder="Harga"
                              value="<?php echo $harga; ?>" />
                    </div>
               </div>
	 <div class="row mb-3">
                    <label for="link_produk" class="col-sm-3
                         col-form-label">Link Produk
                         <?php echo form_error('link_produk') ?></label>
                    <div class="col-sm-9">
                         <textarea class="form-control" rows="3" name="link_produk"
                              id="link_produk" rows="3"
                              placeholder="Link Produk"><?php echo $link_produk; ?></textarea>
                    </div>
               </div>
	 <input type="hidden" name="id_alat" value="<?php echo $id_alat; ?>" /> 
	 <button type="submit" class="btn btn-primary"><i class="bx bx-save"></i>
                    <?php echo $button ?></button> 
	 <a href="<?php echo site_url('alat') ?>" class="btn btn-outline-info"><i
                         class="bx bx-exit"></i> Cancel</a>
	
               </div>
               </div>
               </div>
               </form>
               