<div class="sidebar-wrapper" data-simplebar="true">
     <div class="sidebar-header">
          <div>
               <img src="assets/logo.png" style="widht: 60px;height: 30px;" alt="logo icon">
          </div>
          <div>
               <!-- <h4 class="logo-text" style="color: #CC9C27">Zam Zam</h4> -->
          </div>
          <div class="toggle-icon ms-auto" style="color: #CC9C27"><i class='bx bx-arrow-to-left'></i>
          </div>
     </div>
     <!--navigation-->
     <ul class="metismenu" id="menu">
          <li class="menu-label">Main Menu</li>
          <li>
               <a href="app">
                    <div class="parent-icon"><i class='bx bx-home-circle'></i>
                    </div>
                    <div class="menu-title">Dashboard</div>
               </a>
          </li>
          <li>
               <a href="ksm">
                    <div class="parent-icon"><i class='bx bx-file'></i>
                    </div>
                    <div class="menu-title">Data KSM</div>
               </a>
          </li>
		<li>
               <a href="tindakan">
                    <div class="parent-icon"><i class='bx bx-box'></i>
                    </div>
                    <div class="menu-title">Data Tindakan</div>
               </a>
          </li>

          <li>
               <a href="bahan">
                    <div class="parent-icon"><i class='bx bx-buildings'></i>
                    </div>
                    <div class="menu-title">Data Bahan</div>
               </a>
          </li>
          <li>
               <a href="alat">
                    <div class="parent-icon"><i class='bx bx-highlight'></i>
                    </div>
                    <div class="menu-title">Data Alat</div>
               </a>
          </li>

          <!-- <li>
               <a href="biaya_tidak_langsung">
                    <div class="parent-icon"><i class='bx bx-movie'></i>
                    </div>
                    <div class="menu-title">Biaya Tidak Langsung</div>
               </a>
          </li> -->

          <li>
               <a href="javascript:;" class="has-arrow">
                    <div class="parent-icon"><i class='bx bx-cart'></i>
                    </div>
                    <div class="menu-title">Biaya Tidak Langsung</div>
               </a>
               <ul>
                    <li> 
                         <a href="biaya_tidak_langsung"><i class="bx bx-right-arrow-alt"></i>All Data</a>
                    </li>
                    <?php foreach ($this->db->get('biaya_tidak_langsung')->result() as $rw): ?>
                         <li> 
                              <a href="biaya_tidak_langsung?id_btl=<?php echo $rw->id_btl ?>"><i class="bx bx-right-arrow-alt"></i><?php echo $rw->nama ?></a>
                         </li>
                    <?php endforeach ?>
               </ul>
          </li>

          <li>
               <a href="bahan_tindakan">
                    <div class="parent-icon"><i class='bx bx-capsule'></i>
                    </div>
                    <div class="menu-title">Bahan Tindakan</div>
               </a>
          </li>

          <li>
               <a href="alat_tindakan">
                    <div class="parent-icon"><i class='bx bx-list-ul'></i>
                    </div>
                    <div class="menu-title">Alat Tindakan</div>
               </a>
          </li>

          <li>
               <a href="app/cetak_view">
                    <div class="parent-icon"><i class='bx bx-printer'></i>
                    </div>
                    <div class="menu-title">Tarif</div>
               </a>
          </li>

          <li>
               <a href="app/rekapitulasi">
                    <div class="parent-icon"><i class='bx bx-printer'></i>
                    </div>
                    <div class="menu-title">Rekapitulasi</div>
               </a>
          </li>
		<?php if($this->session->userdata('level') == 'superadmin'): ?>
		<li>
               <a href="app_user">
                    <div class="parent-icon"><i class='bx bx-user-circle'></i>
                    </div>
                    <div class="menu-title">Master User</div>
               </a>
          </li>
          <?php endif ?>
     </ul>
     <!--end navigation-->
</div>