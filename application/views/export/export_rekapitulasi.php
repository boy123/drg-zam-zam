<?php 
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=export_rekapitulasi.xls");
?>
<table border="1">
                    <thead>
                         <tr>
                              <th>NO</th>
                              <th>KODE TINDAKAN</th>
                              <th>NAMA TINDAKAN</th>
                              <th>KSM</th>
                              <th>TOTAL FORMULARIUM BAHAN PER PEMAKAIAN</th>
                              <th>TOTAL FORMULARIUM ALAT PER PEMAKAIAN</th>
                              <th>TOTAL BIAYA TIDAK LANGSUNG</th>
                              <!-- <th>TOTAL FORMULARIUM BIAYA LAIN</th> -->
                              <th>TOTAL TARIF</th>
                         </tr>
                    </thead>
                    <tbody><?php
                        $no = 1;
                         $id_ksm_user = $this->session->userdata('ksm');
                         $ksm_user = get_data('ksm','id_ksm',$id_ksm_user,'ksm');
                         $where = "";
                         if ($ksm_user != '') {
                              $where = "WHERE ksm='$ksm_user'";
                         }
                        $sql = $this->db->query("SELECT * FROM tindakan $where");
                        foreach ($sql->result() as $rw)
                        {
                            ?>
                         <tr>
                              <td width="80px"><?php echo $no ?></td>
                              <td><?php echo $rw->kode_tindakan ?></td>
                              <td><?php echo $rw->nama_tindakan ?></td>
                              <td><?php echo $rw->ksm ?></td>
                              <td><?php $x = total_b_p($rw->kode_tindakan); echo number_format($x,2,',','.'); ?></td>
                              <td><?php $y = total_a_p($rw->kode_tindakan); echo number_format($y,2,',','.'); ?></td>
                              <!-- <td><?php $z = total_btl() * ($rw->pembobotan_biaya_tidak_langsung) / $rw->total_tindakan; echo number_format($z,2,',','.'); $total = $total + $z ?></td> -->
                              <td>
                                   <?php 
                                   $z = ($rw->biaya_tidak_langsung_pertindakan =='' or $rw->biaya_tidak_langsung_pertindakan == null or $rw->biaya_tidak_langsung_pertindakan == 0) ? 0 : $rw->biaya_tidak_langsung_pertindakan;
                                   echo number_format($z,2,',','.');
                                    ?>
                              </td>
                              <!-- <td><?php echo total_a_p($rw->kode_tindakan) ?></td> -->
                              <td>
                                   <?php 
                                   $total = $x + $y + $z;
                                   echo number_format($total,2,',','.'); 
                                   ?>
                                   
                              </td>
                              
                         </tr>
                         <?php
                            $no++;
                        }
                        ?>
                    </tbody>
               </table>