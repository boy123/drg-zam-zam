<?php 
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=export_bahan.xls");
?>
<table border="1">
<thead>
                         <tr>
                              <th>ID Bahan</th>
                              <th>Nama Bahan</th>
                              <th>Volume</th>
                              <th>Satuan</th>
                              <th>Merk</th>
                              <th>Harga</th>
                              <th>Link Produk</th>
                              <th>Action</th>
                         </tr>
                    </thead>
                    <tbody><?php
                        $bahan_data = $this->db->get('bahan');
                        foreach ($bahan_data->result() as $bahan)
                        {
                            ?>
                         <tr>
                              <td><?php echo $bahan->id_bahan ?></td>
                              <td><?php echo $bahan->nama_bahan ?></td>
                              <td><?php echo $bahan->volume ?></td>
                              <td><?php echo $bahan->satuan ?></td>
                              <td><?php echo $bahan->merk ?></td>
                              <td><?php echo $bahan->harga ?></td>
                              <td>
                                   <a href="<?php echo $bahan->link_produk ?>"><?php echo $bahan->link_produk ?></a>
                              </td>
                              
                         </tr>
                         <?php
                        }
                        ?>
                    </tbody>
</table>