<?php 
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=export_alat.xls");
?>
<table border="1">
	<thead>
                         <tr>
                              <th>ID ALAT</th>
                              <th>Nama Alat</th>
                              <th>Volume</th>
                              <th>Satuan</th>
                              <th>Merk</th>
                              <th>Harga</th>
                              <th>Link Produk</th>
                              <th>Action</th>
                         </tr>
                    </thead>
                    <tbody><?php
                        $alat_data = $this->db->get('alat');
                        foreach ($alat_data->result() as $alat)
                        {
                            ?>
                         <tr>
                              <td><?php echo $alat->id_alat ?></td>
                              <td><?php echo $alat->nama_alat ?></td>
                              <td><?php echo $alat->volume ?></td>
                              <td><?php echo $alat->satuan ?></td>
                              <td><?php echo $alat->merk ?></td>
                              <td><?php echo $alat->harga ?></td>
                              <td>
                                   <a href="<?php echo $alat->link_produk ?>"><?php echo $alat->link_produk ?></a>
                              </td>
                              
                         </tr>
                         <?php
                        }
                        ?>
                    </tbody>
</table>