<div class="card">
     <div class="card-body">
          <div class="row mb-4">
               <div class="col">
                    <a href="biaya_tidak_langsung/create" class="btn btn-primary"><i
                              class="bx bx-plus mr-1"></i>Tambah</a>
                    <button type="button" class="btn btn-warning" data-bs-toggle="modal"
                         data-bs-target="#importModal"><i class="bx bx-upload mr-1"></i>Import Data</button>
                    <!-- Modal -->
                    <div class="modal fade" id="importModal" tabindex="-1" aria-labelledby="importModal"
                         aria-hidden="true">
                         <div class="modal-dialog">
                              <form action="biaya_tidak_langsung/import_excel" method="post"
                                   enctype="multipart/form-data">
                                   <div class="modal-content">
                                        <div class="modal-header">
                                             <h5 class="modal-title" id="importModal">Import Data</h5>
                                             <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                  aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                             <div class="row mb-3">
                                                  <div class="col-sm-9">
                                                       <a href="files/excel/import_biaya_tidak_langsung.xlsx"
                                                            class="badge bg-success">Download Template</a>
                                                  </div>
                                             </div>
                                             <div class="row mb-3">
                                                  <label class="col-sm-3 col-form-label">File</label>
                                                  <div class="col-sm-9">
                                                       <input class="form-control" type="file" name="file_excel"
                                                            id="formFile">
                                                  </div>
                                             </div>
                                        </div>
                                        <div class="modal-footer">
                                             <button type="button" class="btn btn-secondary"
                                                  data-bs-dismiss="modal">Close</button>
                                             <button type="submit" class="btn btn-primary">Import</button>
                                        </div>
                                   </div>
                              </form>
                         </div>
                    </div>
               </div>
          </div>
          <div class="row mb-3">
               <div class="col">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
               </div>
          </div>
          <div class="table-responsive">
               <table id="exampleDataTable" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                         <tr>
                              <th>No</th>
                              <th>Nama</th>
                              <th>Satuan</th>
                              <th>Biaya</th>
                              <th>Kebutuhan Perbulan</th>
                              <th>Biaya Pertahun</th>
                              <th>Action</th>
                         </tr>
                    </thead>
                    <tbody><?php
                        $no = 1;
                        if (isset($_GET['id_btl'])) {
                             $this->db->where('id_btl', $this->input->get('id_btl'));
                        } 
                        $biaya_tidak_langsung_data = $this->db->get('biaya_tidak_langsung');
                        foreach ($biaya_tidak_langsung_data->result() as $biaya_tidak_langsung)
                        {
                            ?>
                         <tr>
                              <td width="80px"><?php echo $no ?></td>
                              <td><?php echo $biaya_tidak_langsung->nama ?></td>
                              <td><?php echo $biaya_tidak_langsung->satuan ?></td>
                              <td><?php echo $biaya_tidak_langsung->biaya ?></td>
                              <td><?php echo $biaya_tidak_langsung->kebutuhan_perbulan ?></td>
                              <td><?php echo number_format($biaya_tidak_langsung->biaya_pertahun,2,',','.'); ?>
                              </td>
                              <td style="text-align:center" width="200px">

                                   <a href="biaya_tidak_langsung/update/<?php echo $biaya_tidak_langsung->id_btl ?>"
                                        title="Update Data" class="btn btn-sm btn-primary"><i
                                             class="bx bx-edit me-0"></i>
                                   </a>
                                   <a href="biaya_tidak_langsung/delete/<?php echo $biaya_tidak_langsung->id_btl ?>"
                                        title="Hapus Data"
                                        onclick="javasciprt: return confirm('Yakin akan hapus data ini ?')"
                                        class="btn btn-sm btn-danger"><i class="bx bx-trash-alt me-0"></i>
                                   </a>

                              </td>
                         </tr>
                         <?php
                            $no++;
                        }
                        ?>
                    </tbody>
                    <tfoot>
                         <tr>
                              <td colspan="5"><b>Total</b></td>
                              <td colspan="2"><b><?php echo number_format(total_btl(),2,',','.') ?></b></td>
                         </tr>
                    </tfoot>
               </table>
          </div>
          <?php if ($this->session->userdata('level') == 'superadmin'): ?>
          <a href="app/reset_data/biaya_tidak_langsung" onclick="javasciprt: return confirm('Yakin akan hapus data ini ?')" class="btn btn-danger">Reset All Data</a>
     <?php endif ?>
     </div>
</div>