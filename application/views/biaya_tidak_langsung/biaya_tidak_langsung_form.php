<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
	 <div class="card border-top border-0 border-4 border-info">
     <div class="card-body">
          <div class="border p-4 rounded">
               <div class="card-title d-flex align-items-center">
                    <div><i class="bx bx-edit-alt me-1 font-22 text-info"></i>
                    </div>
                    <h5 class="mb-0 text-info"><?php echo $judul_form ?></h5>
               </div>
               <hr />
               
	 <div class="row mb-3">
                    <label for="nama" class="col-sm-3
                         col-form-label">Nama
                         <?php echo form_error('nama') ?></label>
                    <div class="col-sm-9">
                         <input type="text" class="form-control" name="nama"
                              id="nama" placeholder="Nama"
                              value="<?php echo $nama; ?>" />
                    </div>
               </div>
	 <div class="row mb-3">
                    <label for="satuan" class="col-sm-3
                         col-form-label">Satuan
                         <?php echo form_error('satuan') ?></label>
                    <div class="col-sm-9">
                         <input type="text" class="form-control" name="satuan"
                              id="satuan" placeholder="Satuan"
                              value="<?php echo $satuan; ?>" />
                    </div>
               </div>
	 <div class="row mb-3">
                    <label for="biaya" class="col-sm-3
                         col-form-label">Biaya
                         <?php echo form_error('biaya') ?></label>
                    <div class="col-sm-9">
                         <input type="text" class="form-control" name="biaya"
                              id="biaya" placeholder="Biaya" onkeyup="getBiaya()"
                              value="<?php echo $biaya; ?>" />
                    </div>
               </div>
	 <div class="row mb-3">
                    <label for="kebutuhan_perbulan" class="col-sm-3
                         col-form-label">Kebutuhan Perbulan
                         <?php echo form_error('kebutuhan_perbulan') ?></label>
                    <div class="col-sm-9">
                         <input type="text" class="form-control" onkeyup="getBiaya()" name="kebutuhan_perbulan"
                              id="kebutuhan_perbulan" placeholder="Kebutuhan Perbulan"
                              value="<?php echo $kebutuhan_perbulan; ?>" />
                    </div>
               </div>
	 <div class="row mb-3">
                    <label for="biaya_pertahun" class="col-sm-3
                         col-form-label">Biaya Pertahun
                         <?php echo form_error('biaya_pertahun') ?></label>
                    <div class="col-sm-9">
                         <input type="text" class="form-control" name="biaya_pertahun"
                              id="biaya_pertahun" placeholder="Biaya Pertahun"
                              value="<?php echo $biaya_pertahun; ?>" />
                    </div>
               </div>
	 <input type="hidden" name="id_btl" value="<?php echo $id_btl; ?>" /> 
	 <button type="submit" class="btn btn-primary"><i class="bx bx-save"></i>
                    <?php echo $button ?></button> 
	 <a href="<?php echo site_url('biaya_tidak_langsung') ?>" class="btn btn-outline-info"><i
                         class="bx bx-exit"></i> Cancel</a>
	
               </div>
               </div>
               </div>
               </form>

<script type="text/javascript">
     function getBiaya() {
          var hasil = parseInt($("#biaya").val()) * parseInt($("#kebutuhan_perbulan").val()) * 12;
          $("#biaya_pertahun").val(hasil);
     }
</script>
               