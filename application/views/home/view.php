<div class="row mb-3">
     <div class="col">
          <?php echo message("info","Masih Dalam Pengembangan ya !!") ?>
     </div>
</div>
<div class="row row-cols-1 row-cols-md-2 row-cols-xl-4">
     <div class="col">
          <div class="card radius-10 border-start border-0 border-3 border-info">
               <div class="card-body">
                    <div class="d-flex align-items-center">
                         <div>
                              <p class="mb-0 text-secondary">Bahan</p>
                              <h4 class="my-1 text-info"><?php echo $this->db->get('bahan')->num_rows(); ?></h4>
                         </div>
                         <div class="widgets-icons-2 rounded-circle bg-gradient-scooter text-white ms-auto">
                              <i class="bx bxs-bar-chart-alt-2"></i>
                         </div>
                    </div>
               </div>
          </div>
     </div>
     <div class="col">
          <div class="card radius-10 border-start border-0 border-3 border-danger">
               <div class="card-body">
                    <div class="d-flex align-items-center">
                         <div>
                              <p class="mb-0 text-secondary">Alat</p>
                              <h4 class="my-1 text-danger"><?php echo $this->db->get('alat')->num_rows(); ?></h4>
                              <!-- <p class="mb-0 font-13">+5.4% from last week</p> -->
                         </div>
                         <div class="widgets-icons-2 rounded-circle bg-gradient-bloody text-white ms-auto">
                              <i class="bx bxs-bar-chart-alt-2"></i>
                         </div>
                    </div>
               </div>
          </div>
     </div>
     <div class="col">
          <div class="card radius-10 border-start border-0 border-3 border-success">
               <div class="card-body">
                    <div class="d-flex align-items-center">
                         <div>
                              <p class="mb-0 text-secondary">Bahan Tindakan</p>
                              <h4 class="my-1 text-success"><?php echo $this->db->get('bahan_tindakan')->num_rows(); ?></h4>
                              <!-- <p class="mb-0 font-13">-4.5% from last week</p> -->
                         </div>
                         <div class="widgets-icons-2 rounded-circle bg-gradient-ohhappiness text-white ms-auto">
                              <i class="bx bxs-bar-chart-alt-2"></i>
                         </div>
                    </div>
               </div>
          </div>
     </div>
     <div class="col">
          <div class="card radius-10 border-start border-0 border-3 border-warning">
               <div class="card-body">
                    <div class="d-flex align-items-center">
                         <div>
                              <p class="mb-0 text-secondary">Alat Tindakan</p>
                              <h4 class="my-1 text-warning"><?php echo $this->db->get('alat_tindakan')->num_rows(); ?></h4>
                              <!-- <p class="mb-0 font-13">+8.4% from last week</p> -->
                         </div>
                         <div class="widgets-icons-2 rounded-circle bg-gradient-blooker text-white ms-auto">
                              <i class="bx bxs-bar-chart-alt-2"></i>
                         </div>
                    </div>
               </div>
          </div>
     </div>
     <div class="col">
          <div class="card radius-10 border-start border-0 border-3 border-warning">
               <div class="card-body">
                    <div class="d-flex align-items-center">
                         <div>
                              <p class="mb-0 text-secondary">Akumulasi Total Tindakan</p>
                              <h4 class="my-1 text-warning"><?php echo total_tindakan(); ?></h4>
                              <!-- <p class="mb-0 font-13">+8.4% from last week</p> -->
                         </div>
                         <div class="widgets-icons-2 rounded-circle bg-gradient-blooker text-white ms-auto">
                              <i class="bx bxs-bar-chart-alt-2"></i>
                         </div>
                    </div>
               </div>
          </div>
     </div>
</div>
<!--end row-->
